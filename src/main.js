var rotation = []
var align = []
var leftOffset = []
var rightOffset = []
var topOffset = []
var zIndex = 0
for (var i = 0; i < 367; i++) {
  if (rotation[i] === undefined) {
    rotation[i] = -8 + Math.random() * 16
  }
  if (align[i] === undefined) {
    align[i] = Math.floor(Math.random() * 2)
  }
  if (leftOffset[i] === undefined) {
    leftOffset[i] = -1 + Math.random() * 15
  }
  if (rightOffset[i] === undefined) {
    rightOffset[i] = -1 + Math.random() * 15
  }
  if (topOffset[i] === undefined) {
    topOffset[i] = -6 + Math.random() * 80
  }
}

var imageTextLink = [1, 2, 3, 4, 5, 6, 7, [8, 9], 10, [11, 12], 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, [29, 30], 31, 32, 33, 34, 35, [36, 37], 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, [48, 49, 50], 51, 52, 53, 54, 55, 56, [57, 58], 59, [60, 61],
  [62, 63], 64, 65, [66, 67], 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, [80, 81], 82, [83, 84],
  [85, 86], 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, [103, 104], 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, [128, 129], 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, [148, 149], 150, 151, 152, 153, 154, 155, [156, 157], 158, 159, 160, 161, 162, 163, [164, 165], 166, 167, [168, 169], 170, 171, 172, 173, 174, 175, 176, 177, 178, [179, 180], 181, 182, 183, 184, 185, [186, 187], 188, 189, 190, 191, [192, 193], 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, [245, 246], 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, [272, 273], 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, [289, 290, 291], 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, [307, 308], 309, 310, 311, 312, 313, 314, [315, 316], 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366
]
var textToImage = []
var pageCounter = 1

function createTextHover(i) {
  return function() {
    if (imageTextLink[i].length === undefined) {
      document.getElementById("hover-img" + imageTextLink[i]).addEventListener("mouseover", function() {
        if (document.getElementById("img" + imageTextLink[i]).style.visibility !== "visible") {
          document.getElementById("img" + imageTextLink[i]).style['-webkit-transition'] = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i]).style.transition = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i]).style.opacity = 1
          document.getElementById("img" + imageTextLink[i]).style.visibility = 'visible'
        }
        zIndex++
        document.getElementById("img" + imageTextLink[i]).style['z-index'] = zIndex + 1
        document.getElementById("img" + imageTextLink[i]).style.position = 'absolute'
        if (align[imageTextLink[i] - 1] === 0) {
          document.getElementById("img" + imageTextLink[i]).style.left = leftOffset[imageTextLink[i] - 1] + '%'
        } else {
          document.getElementById("img" + imageTextLink[i]).style.right = rightOffset[imageTextLink[i] - 1] + '%'
        }
        document.getElementById("img" + imageTextLink[i]).style.top = topOffset[imageTextLink[i] - 1] + '%'
        document.getElementById("img" + imageTextLink[i]).style['-webkit-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i]).style['-moz-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i]).style.boxShadow = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i]).style['-ms-transform'] = 'rotate(' + rotation[imageTextLink[i] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i]).style.transform = 'rotate(' + rotation[imageTextLink[i] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i]).style.border = '6px solid #fff'
      })
    } else if (imageTextLink[i].length === 2) {
      document.getElementById("hover-img" + imageTextLink[i][0]).addEventListener("mouseover", function() {
        if (document.getElementById("img" + imageTextLink[i][0]).style.visibility !== "visible") {
          document.getElementById("img" + imageTextLink[i][0]).style['-webkit-transition'] = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i][0]).style.transition = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i][0]).style.opacity = 1
          document.getElementById("img" + imageTextLink[i][0]).style.visibility = 'visible'
        }
        zIndex++
        document.getElementById("img" + imageTextLink[i][0]).style['z-index'] = zIndex + 1
        document.getElementById("img" + imageTextLink[i][0]).style.position = 'absolute'
        if (align[imageTextLink[i][0] - 1] === 0) {
          document.getElementById("img" + imageTextLink[i][0]).style.left = leftOffset[imageTextLink[i][0] - 1] + '%'
        } else {
          document.getElementById("img" + imageTextLink[i][0]).style.right = rightOffset[imageTextLink[i][0] - 1] + '%'
        }
        document.getElementById("img" + imageTextLink[i][0]).style.top = topOffset[imageTextLink[i][0] - 1] + '%'
        document.getElementById("img" + imageTextLink[i][0]).style['-webkit-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][0]).style['-moz-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][0]).style.boxShadow = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][0]).style['-ms-transform'] = 'rotate(' + rotation[imageTextLink[i][0] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i][0]).style.transform = 'rotate(' + rotation[imageTextLink[i][0] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i][0]).style.border = '6px solid #fff'
        if (document.getElementById("img" + imageTextLink[i][1]).style.visibility !== "visible") {
          document.getElementById("img" + imageTextLink[i][1]).style['-webkit-transition'] = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i][1]).style.transition = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i][1]).style.opacity = 1
          document.getElementById("img" + imageTextLink[i][1]).style.visibility = 'visible'
        }
        zIndex++
        document.getElementById("img" + imageTextLink[i][1]).style['z-index'] = zIndex + 1
        document.getElementById("img" + imageTextLink[i][1]).style.position = 'absolute'
        if (align[imageTextLink[i][1] - 1] === 0) {
          document.getElementById("img" + imageTextLink[i][1]).style.left = leftOffset[imageTextLink[i][1] - 1] + '%'
        } else {
          document.getElementById("img" + imageTextLink[i][1]).style.right = rightOffset[imageTextLink[i][1] - 1] + '%'
        }
        document.getElementById("img" + imageTextLink[i][1]).style.top = topOffset[imageTextLink[i][1] - 1] + '%'
        document.getElementById("img" + imageTextLink[i][1]).style['-webkit-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][1]).style['-moz-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][1]).style.boxShadow = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][1]).style['-ms-transform'] = 'rotate(' + rotation[imageTextLink[i][1] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i][1]).style.transform = 'rotate(' + rotation[imageTextLink[i][1] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i][1]).style.border = '6px solid #fff'
      })
    } else if (imageTextLink[i].length === 3) {
      document.getElementById("hover-img" + imageTextLink[i][0]).addEventListener("mouseover", function() {
        if (document.getElementById("img" + imageTextLink[i][0]).style.visibility !== "visible") {
          document.getElementById("img" + imageTextLink[i][0]).style['-webkit-transition'] = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i][0]).style.transition = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i][0]).style.opacity = 1
          document.getElementById("img" + imageTextLink[i][0]).style.visibility = 'visible'
        }
        zIndex++
        document.getElementById("img" + imageTextLink[i][0]).style['z-index'] = zIndex + 1
        document.getElementById("img" + imageTextLink[i][0]).style.position = 'absolute'
        if (align[imageTextLink[i][0] - 1] === 0) {
          document.getElementById("img" + imageTextLink[i][0]).style.left = leftOffset[imageTextLink[i][0] - 1] + '%'
        } else {
          document.getElementById("img" + imageTextLink[i][0]).style.right = rightOffset[imageTextLink[i][0] - 1] + '%'
        }
        document.getElementById("img" + imageTextLink[i][0]).style.top = topOffset[imageTextLink[i][0] - 1] + '%'
        document.getElementById("img" + imageTextLink[i][0]).style['-webkit-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][0]).style['-moz-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][0]).style.boxShadow = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][0]).style['-ms-transform'] = 'rotate(' + rotation[imageTextLink[i][0] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i][0]).style.transform = 'rotate(' + rotation[imageTextLink[i][0] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i][0]).style.border = '6px solid #fff'
        if (document.getElementById("img" + imageTextLink[i][1]).style.visibility !== "visible") {
          document.getElementById("img" + imageTextLink[i][1]).style['-webkit-transition'] = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i][1]).style.transition = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i][1]).style.opacity = 1
          document.getElementById("img" + imageTextLink[i][1]).style.visibility = 'visible'
        }
        zIndex++
        document.getElementById("img" + imageTextLink[i][1]).style['z-index'] = zIndex + 1
        document.getElementById("img" + imageTextLink[i][1]).style.position = 'absolute'
        if (align[imageTextLink[i][1] - 1] === 0) {
          document.getElementById("img" + imageTextLink[i][1]).style.left = leftOffset[imageTextLink[i][1] - 1] + '%'
        } else {
          document.getElementById("img" + imageTextLink[i][1]).style.right = rightOffset[imageTextLink[i][1] - 1] + '%'
        }
        document.getElementById("img" + imageTextLink[i][1]).style.top = topOffset[imageTextLink[i][1] - 1] + '%'
        document.getElementById("img" + imageTextLink[i][1]).style['-webkit-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][1]).style['-moz-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][1]).style.boxShadow = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][1]).style['-ms-transform'] = 'rotate(' + rotation[imageTextLink[i][1] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i][1]).style.transform = 'rotate(' + rotation[imageTextLink[i][1] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i][1]).style.border = '6px solid #fff'
        if (document.getElementById("img" + imageTextLink[i][2]).style.visibility !== "visible") {
          document.getElementById("img" + imageTextLink[i][2]).style['-webkit-transition'] = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i][2]).style.transition = 'opacity 1500ms'
          document.getElementById("img" + imageTextLink[i][2]).style.opacity = 1
          document.getElementById("img" + imageTextLink[i][2]).style.visibility = 'visible'
        }
        zIndex++
        document.getElementById("img" + imageTextLink[i][2]).style['z-index'] = zIndex + 1
        document.getElementById("img" + imageTextLink[i][2]).style.position = 'absolute'
        if (align[imageTextLink[i][2] - 1] === 0) {
          document.getElementById("img" + imageTextLink[i][2]).style.left = leftOffset[imageTextLink[i][2] - 1] + '%'
        } else {
          document.getElementById("img" + imageTextLink[i][2]).style.right = rightOffset[imageTextLink[i][2] - 1] + '%'
        }
        document.getElementById("img" + imageTextLink[i][2]).style.top = topOffset[imageTextLink[i][2] - 1] + '%'
        document.getElementById("img" + imageTextLink[i][2]).style['-webkit-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][2]).style['-moz-box-shadow'] = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][2]).style.boxShadow = '-3px 9px 34px 1px rgba(0,0,0,0.49)'
        document.getElementById("img" + imageTextLink[i][2]).style['-ms-transform'] = 'rotate(' + rotation[imageTextLink[i][2] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i][2]).style.transform = 'rotate(' + rotation[imageTextLink[i][2] - 1] + 'deg)'
        document.getElementById("img" + imageTextLink[i][2]).style.border = '6px solid #fff'
      })
    }
  }
}
for (var i = 0; i < imageTextLink.length; i++) {
  textToImage[i] = createTextHover(i)
}
for (var j = 0; j < imageTextLink.length; j++) {
  textToImage[j]()
}

var imageInformation = []

function createImageHover(k) {
  return function() {
    document.getElementById("img" + (k + 1)).addEventListener("mouseover", function() {
      zIndex++
      document.getElementById("img" + (k + 1)).style['-webkit-transition'] = 'all 1500ms'
      document.getElementById("img" + (k + 1)).style.transition = 'all 1500ms'
      document.getElementById("img" + (k + 1)).style['-webkit-transform'] = 'scale(1.5)'
      document.getElementById("img" + (k + 1)).style.transform = 'scale(1.5)'
      document.getElementById("img" + (k + 1)).style['z-index'] = zIndex + 1

      document.getElementById("information" + (k + 1)).style['-webkit-transition'] = 'all 2000ms'
      document.getElementById("information" + (k + 1)).style.transition = 'all 2000ms'
      document.getElementById("information" + (k + 1)).style.visibility = 'visible'
      document.getElementById("information" + (k + 1)).style.opacity = 1
    })
    document.getElementById("img" + (k + 1)).addEventListener("mouseout", function() {
      zIndex++
      document.getElementById("img" + (k + 1)).style['-webkit-transition'] = 'all 1500ms'
      document.getElementById("img" + (k + 1)).style.transition = 'all 1500ms'
      document.getElementById("img" + (k + 1)).style['-webkit-transform'] = 'scale(1)'
      document.getElementById("img" + (k + 1)).style.transform = 'scale(1)'
      document.getElementById("img" + (k + 1)).style['-ms-transform'] = 'rotate(' + rotation[k] + 'deg)'
      document.getElementById("img" + (k + 1)).style.transform = 'rotate(' + rotation[k] + 'deg)'
      document.getElementById("img" + (k + 1)).style['z-index'] = zIndex + 1

      document.getElementById("information" + (k + 1)).style['-webkit-transition'] = 'all 2000ms'
      document.getElementById("information" + (k + 1)).style.transition = 'all 2000ms'
      document.getElementById("information" + (k + 1)).style.visibility = 'hidden'
      document.getElementById("information" + (k + 1)).style.opacity = 0
    })
  }
}
for (var k = 0; k < 367; k++) {
  imageInformation[k] = createImageHover(k)
}
for (var l = 0; l < 367; l++) {
  imageInformation[l]()
}

document.getElementById("page_01").addEventListener("mouseover", function() {
  for (var i = 10; i < 28; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_02").addEventListener("mouseover", function() {
  for (var i = 0; i < 10; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 28; i < 44; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_03").addEventListener("mouseover", function() {
  for (var i = 10; i < 28; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 44; i < 72; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_04").addEventListener("mouseover", function() {
  for (var i = 28; i < 44; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 71; i < 88; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_05").addEventListener("mouseover", function() {
  for (var i = 44; i < 71; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 88; i < 104; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_06").addEventListener("mouseover", function() {
  for (var i = 71; i < 88; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 104; i < 119; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_07").addEventListener("mouseover", function() {
  for (var i = 88; i < 104; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 119; i < 131; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_08").addEventListener("mouseover", function() {
  for (var i = 104; i < 119; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 131; i < 145; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_09").addEventListener("mouseover", function() {
  for (var i = 119; i < 131; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 145; i < 155; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_10").addEventListener("mouseover", function() {
  for (var i = 131; i < 145; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 155; i < 169; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_11").addEventListener("mouseover", function() {
  for (var i = 145; i < 155; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 169; i < 188; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_12").addEventListener("mouseover", function() {
  for (var i = 155; i < 169; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 188; i < 214; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_13").addEventListener("mouseover", function() {
  for (var i = 169; i < 188; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 214; i < 239; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_14").addEventListener("mouseover", function() {
  for (var i = 188; i < 214; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 237; i < 262; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_15").addEventListener("mouseover", function() {
  for (var i = 214; i < 237; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 262; i < 286; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_16").addEventListener("mouseover", function() {
  for (var i = 239; i < 262; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 286; i < 311; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_17").addEventListener("mouseover", function() {
  for (var i = 262; i < 286; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 311; i < 330; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_18").addEventListener("mouseover", function() {
  for (var i = 286; i < 311; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 330; i < 340; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_19").addEventListener("mouseover", function() {
  for (var i = 318; i < 330; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 340; i < 355; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_20").addEventListener("mouseover", function() {
  for (var i = 330; i < 340; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
  for (var i = 355; i < 367; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
document.getElementById("page_21").addEventListener("mouseover", function() {
  for (var i = 340; i < 355; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})

document.getElementById("back").addEventListener("click", function() {
  if (pageCounter === 21) {
    pageCounter = 1
  } else {
    pageCounter++
  }
  if (pageCounter < 10) {
    document.getElementById("page_0" + pageCounter).scrollIntoView({
      behavior: "smooth"
    })
  } else {
    document.getElementById("page_" + pageCounter).scrollIntoView({
      behavior: "smooth"
    })
  }
  for (var i = 0; i < 367; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})

document.getElementById("back").addEventListener("contextmenu", function() {
  if (pageCounter === 1) {
    pageCounter = 1
  } else {
    pageCounter--
  }
  if (pageCounter < 10) {
    document.getElementById("page_0" + pageCounter).scrollIntoView({
      behavior: "smooth"
    })
  } else {
    document.getElementById("page_" + pageCounter).scrollIntoView({
      behavior: "smooth"
    })
  }
  for (var i = 0; i < 367; i++) {
    document.getElementById("img" + (i + 1)).style['-webkit-transition'] = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.transition = 'all 1500ms'
    document.getElementById("img" + (i + 1)).style.opacity = 0
    document.getElementById("img" + (i + 1)).style.visibility = "hidden"
  }
})
